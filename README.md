# Web Antivirus

Archivos estáticos para la web https://antivirus.tedic.org/

Estos archivos se generan a partir del software Hugo. Si encuentras un error o quieres colaborar en algo, no dudes hacer un "merge request" en el repositorio del generador de sitios Hugo, disponible aquí: https://gitlab.com/tedicpy/hugo-docker

Por más información de este proyecto, accede aquí: https://www.tedic.org/proyecto/antivirus